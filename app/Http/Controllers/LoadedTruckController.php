<?php

namespace App\Http\Controllers;

use App\LoadedTruck;
use App\TruckProduct;
use App\Product;
use Illuminate\Http\Request;

class LoadedTruckController extends Controller
{
    public function store(Request $request)
    {
        $load = floatval($request->get('load')) * 1000; // Get load from payload and convert to kg
        $data = $this->loadTruck($load); // Gonna load the truck now
        $cost = $this->getCost($data); // Get cost of transportation
        $truck = $this->insertTruck($load, $data['sum'], $cost); // Insert truck to database
        $this->insertProducts($truck->id, $data['load']); // Insert truck products to database
        return response()->json(
          [
            'truck' => $truck,
            'products' => $data['load'],
            'sum' => $data['sum'],
            'cost' => $cost
          ]
        );
    }

    public function loadTruck($max)
    {
      $products = Product::orderBy('weight', 'desc')->get(); // Select all products
      $load = []; // Truck payload
      $sum = array_sum(array_column($load, 'weight')); // Current sum of payload
      foreach($products as $product) {
        if($sum + $product->weight <= $max) { // If max load is grater than current sum + new product weight
          array_push($load, $product); // Add to the truck payload
          $sum += $product->weight; // Get new sum of payload
        }
      }
      return ["load" => $load, "sum" => $sum];
    }

    public function getCost($data)
    {
        $load_cost = 1; // Loading cost
        $kg_cost = 0.1; // 1 kg cost of tranportation
        $load = $load_cost * count($data['load']); // Loading cost * how many items in a truck
        $ts = $kg_cost * $data['sum']; // All products sum * 1 kg cost
        return $ts + $load;
    }

    public function insertTruck($load, $sum, $cost)
    {
      $truck = new LoadedTruck;
      $truck->max_load = $load;
      $truck->load = $sum;
      $truck->cost = $cost;
      $truck->save();

      return $truck;
    }

    public function insertProducts($truck, $products)
    {
      foreach ($products as $product) {
        $item = new TruckProduct;
        $item->truck_id = $truck;
        $item->product_id = $product->id;
        $item->save();
      }
    }
}
