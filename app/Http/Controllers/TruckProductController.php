<?php

namespace App\Http\Controllers;

use App\TruckProduct;
use Illuminate\Http\Request;

class TruckProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TruckProduct  $truckProduct
     * @return \Illuminate\Http\Response
     */
    public function show(TruckProduct $truckProduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TruckProduct  $truckProduct
     * @return \Illuminate\Http\Response
     */
    public function edit(TruckProduct $truckProduct)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TruckProduct  $truckProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TruckProduct $truckProduct)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TruckProduct  $truckProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(TruckProduct $truckProduct)
    {
        //
    }
}
