<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TruckProduct extends Model
{
    public $timestamps = false;
}
