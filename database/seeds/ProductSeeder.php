<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $faker = Faker\Factory::create();
        $max = 100000.00;
        $sum = 0.00;

        while($max !== $sum) {
            $weight = $faker->randomFloat(2, 55, 5555);
            $weight = $sum + $weight > $max ? $max - $sum : $weight;
            $this->insertProduct($faker->firstName, $weight);
            $sum += $weight;
        }
    }

    public function insertProduct($name, $weight)
    {
        DB::table('products')->insert([
            'name' => $name,
            'weight' => $weight
        ]);
    }
}
