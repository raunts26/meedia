# Guide

1.  Download files and move files to htdocs
2.  Configure virtualhost
3.  Create duplicate of ".env.example" and name it ".env"
4.  In ".env" file configure database
5.  Open folder on terminal (git bash, cmder)
6.  Run "composer install"
7.  Run "npm install"
8.  Run "php artisan key:generate"
9.  Run "php artisan migrate"
10. Run "php artisan db:seed --class=ProductSeeder", creates random products, with human names
